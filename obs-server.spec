%global do_check 0
%global apache_user apache
%global apache_group apache
%global obs_api_support_scripts obs-api-support.target obs-clockwork.service obs-delayedjob-queue-consistency_check.service obs-delayedjob-queue-default.service


Name:           obs-server
Version:        2.10.15
Release:        5
Summary:        The Open Build Service -- Server Component
License:        GPL-2.0-only OR GPL-3.0-only
URL:            http://www.openbuildservice.org
BuildRoot:      %{_tmppath}/%{name}-%{version}-build
Source0:        https://github.com/openSUSE/open-build-service/archive/%{version}.tar.gz
Source1:        Gemfile.lock.aarch64
Source2:        Gemfile.lock.x86
Patch1:         0001-obs_server-fix-usage-info.patch
Patch2:         fix-obs_productconvert-inputfile-path.patch
Patch4:         0004-remove-systemV-method.patch
Patch5:         0005-obsscheduler-stop.patch
Patch6:         obs_project_update-should-tell-the-usage-but-not-hung-still.patch
Patch7:         Adapt-setup-appliance.sh-for-openeuler.patch

BuildArch:      noarch

BuildRequires:  python3-devel, /usr/bin/xmllint, openssl, perl-BSSolv, perl-Compress-Zlib, xz
BuildRequires:  perl-File-Sync >= 0.10, perl-JSON-XS, perl-Net-SSLeay, perl-Socket-MsgHdr, nodejs
BuildRequires:  perl-TimeDate, perl-XML-Parser, perl-XML-Simple, perl-YAML-LibYAML, procps, tzdata
BuildRequires:  rubygem-bundler, rubygem-rake, obs-bundled-gems, rubygem-bigdecimal, perl(Test::Simple) > 1
BuildRequires:  rubygem-rdoc

Requires(pre):  obs-common
Requires:       build >= 20191114, perl-BSSolv, perl(Date::Parse), diffutils, git-core, patch
Requires:       createrepo_c, dpkg, yum, perl-Compress-Zlib, perl-File-Sync >= 0.10, perl-JSON-XS
Requires:       perl-Net-SSLeay, perl-Socket-MsgHdr, perl-XML-Parser, perl-XML-Simple, perl-YAML-LibYAML
Requires:       cpio, curl, perl-Compress-Zlib, perl-TimeDate, screen, psmisc, bash, binutils, bsdtar
Requires:       util-linux >= 2.16, osc
Requires(post): systemd
Requires(preun): systemd
Requires(postun): systemd

Obsoletes:      obs-productconverter < 2.9
Obsoletes:      obs-source_service < 2.9
Provides:       obs-productconverter = %version
Provides:       obs-source_service = %version
Obsoletes:      obs-devel
Provides:       obs-devel
Obsoletes:      obs-worker
Provides:       obs-worker
Obsoletes:      obs-tests-appliance
Provides:       obs-tests-appliance
Obsoletes:      obs-utils
Provides:       obs-utils
Recommends:     cron, logrotate, obs-service-download_url, obs-service-verify_file, openslp, lvm2

%description
The Open Build Service (OBS) backend is used to store all sources and binaries.
It also calculates the need for new build jobs and distributes it.

%package -n obs-common
Summary:        The Open Build Service -- base configuration files
Requires(pre):  shadow-utils

%description -n obs-common
This is a package providing basic configuration files.

%package -n obs-api
Summary:        The Open Build Service -- The API and WEBUI
Requires:       httpd, apache2-mod_xforward, rubygem-passenger, mod_passenger >= 6.0.4
Requires:       ruby, nodejs, urw-base35-fonts-legacy, obs-api-deps = 2.10.15
Requires:       obs-bundled-gems = 2.10.15
Conflicts:      memcached < 1.4

%description -n obs-api
This is the API server instance, and the web client for the OBS.

%prep
%autosetup -n open-build-service-%version -p1

rm -rf src/{api,backend}/docker-files
rm -rf src/api/Dockerfile.frontend-base
rm -rf src/backend/build
#adpate to obs-bundled-gema-2.10.1 because of updating to obs-server-2.10.11
rm -rf src/api/config/brakeman.ignore
arch=`uname -m`
if [ $arch == 'aarch64' ];then
cp %{SOURCE1} src/api/Gemfile.lock
else
cp %{SOURCE2} src/api/Gemfile.lock
fi

find -name .keep -o -name .gitignore | xargs rm -rf
for file in `grep -Fnr "#!/usr/bin/ruby.ruby2.7" | awk -F':' '{print $1}'`;do sed -i 's:#!/usr/bin/ruby.ruby2.7:#!/usr/bin/ruby:g' $file;done
for file in `grep -Fnr "#!/usr/bin/rake.ruby2.7" | awk -F':' '{print $1}'`;do sed -i 's:#!/usr/bin/rake.ruby2.7:#!/usr/bin/rake:g' $file;done
sed -i 's/apache2.service/httpd.service/g' `find . -name "obsapisetup.service"`
#sed -i 's/2.5.0/2.6.0/g' `find . -name "obs-clockwork.service"`
for file in `find . -name "*.service"`;do sed -i 's:User = wwwrun:User = apache:g' $file;done
for file in `find . -name "*.service"`;do sed -i 's:Group = www:User = apache:g' $file;done
for file in `find . -name "*.service"`;do sed -i 's:/usr/bin/bundle.ruby2.7:/usr/bin/bundle:g' $file;done

%build
export DESTDIR=$RPM_BUILD_ROOT
cd src/api
bundle --local --path %_libdir/obs-api/
cd -
make
sed -i '/c.blacklist_keys = \[/d' src/api/config/initializers/airbrake.rb
sed -i '/config.db_adapter = :/d' src/api/config/initializers/ajax_datatables_rails.rb
sed -i '/AjaxDatatablesRails.configure/d' src/api/config/initializers/ajax_datatables_rails.rb
sed -i '/end/d' src/api/config/initializers/ajax_datatables_rails.rb
rm -rf src/api/app/assets/stylesheets/webui2/webui2.css.scss
sed -i 's/js_compressor = :uglifier/js_compressor = Uglifier.new(harmony: true)/g' src/api/config/environments/production.rb

%install
export DESTDIR=$RPM_BUILD_ROOT
perl -p -i -e 's/^APACHE_VHOST_CONF=.*/APACHE_VHOST_CONF=obs-apache2.conf/' Makefile.include
perl -p -i -e 's/^APACHE_USER=.*/APACHE_USER=apache/' Makefile.include
perl -p -i -e 's/^APACHE_GROUP=.*/APACHE_GROUP=apache/' Makefile.include

mkdir -p src/api/app/assets/config
pushd src/api/app/assets/config
cat << EOF > manifest.js
//= link_tree ../images
//= link_directory ../
//= link_directory ../
EOF
#javascripts.js  stylesheets.css
popd
export OBS_VERSION="%{version}"
DESTDIR=$RPM_BUILD_ROOT make install FILLUPDIR=/var/adm/fillup-templates
if [ -f %{_sourcedir}/open-build-service.obsinfo ]; then
    sed -n -e 's/commit: \(.\+\)/\1/p' %{_sourcedir}/open-build-service.obsinfo > $RPM_BUILD_ROOT/srv/www/obs/api/last_deploy
else
    echo "" > $RPM_BUILD_ROOT/srv/www/obs/api/last_deploy
fi

rm -rf $RPM_BUILD_ROOT/%{_unitdir}/obsclouduploadserver.service
rm -rf $RPM_BUILD_ROOT/%{_unitdir}/obsclouduploadworker.service
rm -rf $RPM_BUILD_ROOT/usr/sbin/rcobsclouduploadserver
rm -rf $RPM_BUILD_ROOT/usr/sbin/rcobsclouduploadworker
rm -rf $RPM_BUILD_ROOT/srv/www/obs/api/spec

if ! test -L $RPM_BUILD_ROOT/usr/lib/obs/server/build; then
  echo "/usr/lib/obs/server/build is not a symlink!"
  exit 1
fi

cd $RPM_BUILD_ROOT/srv/www/obs/api/public/assets/webui2/
ln -sf application-*.js application.js
ln -sf webui2-*.css webui2.css
cd -

[-d $RPM_BUILD_ROOT/etc/sysconfig] || mkdir -p $RPM_BUILD_ROOT/etc/sysconfig
install -m 0644 dist/sysconfig.obs-server $RPM_BUILD_ROOT/etc/sysconfig/obs-server

%if %{do_check}
%check
export DESTDIR=$RPM_BUILD_ROOT
cd $RPM_BUILD_ROOT/usr/lib/obs/server/
rm -rf build
ln -sf /usr/lib/build build
cd -

cd src/backend/
rm -rf build
ln -sf /usr/lib/build build
cd -

cd $RPM_BUILD_ROOT/usr/lib/obs/server/
for i in bs_*; do
  perl -wc "$i"
done
bash $RPM_BUILD_DIR/open-build-service-%version/src/backend/testdata/test_dispatcher || exit 1
cd -

make -C src/backend test
make -C src/api test
make -C dist test
%endif

%pre
getent passwd obsservicerun >/dev/null || \
    /usr/sbin/useradd -r -g obsrun -d /usr/lib/obs -s /sbin/nologin \
    -c "User for the build service source service" obsservicerun

if [ -f /etc/sysconfig/obs-server ] ; then
    . /etc/sysconfig/obs-server
fi
for i in deltastore dispatcher dodup obsgetbinariesproxy publisher rep_server servicedispatch signer src_server warden ; do
    LOG=${OBS_LOG_DIR:=/srv/obs/log}/$i.log
    test -f $LOG && chown obsrun:obsrun $LOG
done
for i in src_service ; do
    LOG=${OBS_LOG_DIR:=/srv/obs/log}/$i.log
    test -f $LOG && chown obsservicerun:obsrun $LOG
done

exit 0

%pre -n obs-common
getent group obsrun >/dev/null || /usr/sbin/groupadd -r obsrun
getent passwd obsrun >/dev/null || \
    /usr/sbin/useradd -r -g obsrun -d /usr/lib/obs -s /sbin/nologin \
    -c "User for build service backend" obsrun
exit 0

%preun
%systemd_preun obsscheduler.service
%systemd_preun obssrcserver.service
%systemd_preun obsrepserver.service
%systemd_preun obspublisher.service
%systemd_preun obssigner.service
%systemd_preun obsservicedispatch.service
%systemd_preun obsservice.service
%systemd_preun obsdeltastore.service
%systemd_preun obsdispatcher.service
%systemd_preun obsdodup.service
%systemd_preun obsgetbinariesproxy.service
%systemd_preun obswarden.service
%systemd_preun obsnotifyforward.service
%systemd_preun obsredis.service
%systemd_preun obsworker.service

%preun -n obs-common
%systemd_preun obsstoragesetup.service

%preun -n obs-api
%systemd_preun %{obs_api_support_scripts}

%post
%systemd_post obsscheduler.service
%systemd_post obssrcserver.service
%systemd_post obsrepserver.service
%systemd_post obspublisher.service
%systemd_post obssigner.service
%systemd_post obsservicedispatch.service
%systemd_post obsservice.service
%systemd_post obsdeltastore.service
%systemd_post obsdispatcher.service
%systemd_post obsdodup.service
%systemd_post obsgetbinariesproxy.service
%systemd_post obswarden.service
%systemd_post obsnotifyforward.service
%systemd_post obsredis.service
%systemd_post obsworker.service

%posttrans
[ -d /srv/obs ] || install -d -o obsrun -g obsrun /srv/obs
if [ -e /usr/lib/obs/server/build -a ! -L /usr/lib/obs/server/build ]; then
  rm -rf /usr/lib/obs/server/build
fi
if [ ! -e /usr/lib/obs/server/build ]; then
  ln -sf ../../build /usr/lib/obs/server/build
fi

%postun
%systemd_postun_with_restart obsscheduler.service
%systemd_postun_with_restart obssrcserver.service
%systemd_postun_with_restart obsrepserver.service
%systemd_postun_with_restart obspublisher.service
%systemd_postun_with_restart obssigner.service
%systemd_postun_with_restart obsservicedispatch.service
%systemd_postun_with_restart obsservice.service
%systemd_postun_with_restart obsdeltastore.service
%systemd_postun_with_restart obsdispatcher.service
%systemd_postun_with_restart obsdodup.service
%systemd_postun_with_restart obsgetbinariesproxy.service
%systemd_postun_with_restart obswarden.service
%systemd_postun_with_restart obsnotifyforward.service
%systemd_postun_with_restart obsredis.service
%systemd_postun_with_restart obsworker.service
%verifyscript -n obs-server
%verify_permissions
rmdir /srv/obs 2> /dev/null || :

%postun -n obs-common
%systemd_postun_with_restart obsstoragesetup.service

%pre -n obs-api
getent group www >/dev/null || /usr/sbin/groupadd -r www

getent passwd wwwrun >/dev/null || /usr/sbin/useradd -d / -g www -s /sbin/nologin -r wwwrun
getent passwd obsapidelayed >/dev/null || \
  /usr/sbin/useradd -r -s /bin/bash -c "User for build service api delayed jobs" -d /srv/www/obs/api -g apache obsapidelayed

if [ "$1" == 2 ]; then
  if [ -e /etc/init.d/rc3.d/S50obsapidelayed ];then
    touch %{_rundir}/enable_obs-api-support.target
  fi
  if systemctl --quiet is-active  obsapidelayed.service;then
    touch %{_rundir}/start_obs-api-support.target
    systemctl stop    obsapidelayed.service
    systemctl disable obsapidelayed.service
  fi
fi

%post -n obs-common
%systemd_post obsstoragesetup.service

%post -n obs-api
if [ -e /srv/www/obs/frontend/config/database.yml ] && [ ! -e /srv/www/obs/api/config/database.yml ]; then
  cp /srv/www/obs/frontend/config/database.yml /srv/www/obs/api/config/database.yml
fi
for i in production.rb ; do
  if [ -e /srv/www/obs/frontend/config/environments/$i ] && [ ! -e /srv/www/obs/api/config/environments/$i ]; then
    cp /srv/www/obs/frontend/config/environments/$i /srv/www/obs/api/config/environments/$i
  fi
done

if [ ! -s /srv/www/obs/api/config/secret.key ]; then
  pushd /srv/www/obs/api >& /dev/null
  RAILS_ENV=production bin/rails secret > /srv/www/obs/api/config/secret.key
  popd >& /dev/null
fi
chmod 0640 /srv/www/obs/api/config/secret.key
chown root.apache /srv/www/obs/api/config/secret.key

sed -i -e 's,[ ]*adapter: mysql$,  adapter: mysql2,' /srv/www/obs/api/config/database.yml
touch /srv/www/obs/api/log/production.log
chown apache:apache /srv/www/obs/api/log/production.log
chown apache:apache /srv/www/obs/api/config/production.sphinx.conf

%systemd_postun_with_restart memcached
%systemd_post %{obs_api_support_scripts}
touch /srv/www/obs/api/last_deploy || true

if [ -e %{_rundir}/enable_obs-api-support.target ];then
  systemctl enable obs-api-support.target
  rm %{_rundir}/enable_obs-api-support.target
fi
if [ -e %{_rundir}/start_obs-api-support.target ];then
  systemctl start  obs-api-support.target
  rm %{_rundir}/start_obs-api-support.target
fi

%postun -n obs-api
%systemd_postun_with_restart %{obs_api_support_scripts}
%systemd_postun_with_restart httpd 

%files
%defattr(-,root,root)
%license COPYING
%doc dist/{README.UPDATERS,README.SETUP} docs/openSUSE.org.xml ReleaseNotes-* README.md AUTHORS
%dir /etc/slp.reg.d
%dir /usr/lib/obs
%dir /usr/lib/obs/server
/usr/lib/obs/server/*
%exclude /usr/lib/obs/server/setup-appliance.sh
%exclude /usr/lib/obs/server/build
/usr/lib/obs/server/BSConfig.pm
/usr/lib/obs/tests/*
%config(noreplace) /etc/logrotate.d/obs-server
%config(noreplace) /etc/sysconfig/obs-server
%{_unitdir}/obsservice.service
%{_unitdir}/obsscheduler.service
%{_unitdir}/obssrcserver.service
%{_unitdir}/obsrepserver.service
%{_unitdir}/obspublisher.service
%{_unitdir}/obssigner.service
%{_unitdir}/obsservicedispatch.service
%{_unitdir}/obsdeltastore.service
%{_unitdir}/obsdispatcher.service
%{_unitdir}/obsdodup.service
%{_unitdir}/obsgetbinariesproxy.service
%{_unitdir}/obswarden.service
%{_unitdir}/obsnotifyforward.service
%{_unitdir}/obsredis.service
%{_unitdir}/obsworker.service
%{_sbindir}/obs_admin
%{_sbindir}/obs_serverstatus
%{_sbindir}/obsscheduler
%{_sbindir}/obsworker
%{_sbindir}/obs_project_update
%{_sbindir}/rcobsdispatcher
%{_sbindir}/rcobspublisher
%{_sbindir}/rcobsrepserver
%{_sbindir}/rcobssrcserver
%{_sbindir}/rcobswarden
%{_sbindir}/rcobsdodup
%{_sbindir}/rcobsgetbinariesproxy
%{_sbindir}/rcobsdeltastore
%{_sbindir}/rcobsservicedispatch
%{_sbindir}/rcobssigner
%{_sbindir}/rcobsnotifyforward
%{_sbindir}/rcobsredis
%{_sbindir}/rcobsworker
%{_sbindir}/rcobsscheduler
%{_sbindir}/rcobsservice
%{_bindir}/obs_productconvert
%config(noreplace) /usr/lib/obs/server/BSConfig.pm
%config(noreplace) /etc/slp.reg.d/*
%config(noreplace) /etc/logrotate.d/obs-source_service
%config(noreplace) /etc/cron.d/cleanup_scm_cache
%ghost /usr/lib/obs/server/build
%attr(0775, obsrun, obsrun) %dir /srv/obs
%attr(0755, obsrun, obsrun) %dir /srv/obs/build
%attr(0755, obsrun, obsrun) %dir /srv/obs/events
%attr(0700, root, root)     %dir /srv/obs/gnupg
%attr(0755, obsrun, obsrun) %dir /srv/obs/info
%attr(0755, obsrun, obsrun) %dir /srv/obs/jobs
%attr(0775, obsrun, obsrun) %dir /srv/obs/log
%attr(0755, obsrun, obsrun) %dir /srv/obs/projects
%attr(0775, obsrun, obsrun) %dir /srv/obs/run
%attr(0755, obsservicerun, obsrun) %dir /srv/obs/service
%attr(0755, obsservicerun, obsrun) %dir /srv/obs/service/log

%triggerin -n obs-server -- docker
usermod -a -G docker obsservicerun

%files -n obs-api
%defattr(-,root,root)
%license COPYING
%doc dist/{README.UPDATERS,README.SETUP} docs/openSUSE.org.xml ReleaseNotes-* README.md AUTHORS
/srv/www/obs/overview
/srv/www/obs/api/config/thinking_sphinx.yml.example
%config(noreplace) /srv/www/obs/api/config/thinking_sphinx.yml
%attr(-,apache,apache)
%config(noreplace) /srv/www/obs/api/config/production.sphinx.conf

%dir /srv/www/obs
%dir /srv/www/obs/api
%dir /srv/www/obs/api/config
%config(noreplace) /srv/www/obs/api/config/cable.yml
%config(noreplace) /srv/www/obs/api/config/feature.yml
%config(noreplace) /srv/www/obs/api/config/puma.rb
%config(noreplace) /srv/www/obs/api/config/secrets.yml
%config(noreplace) /srv/www/obs/api/config/spring.rb
%config(noreplace) /srv/www/obs/api/config/crawler-user-agents.json
/srv/www/obs/api/config/initializers
%dir /srv/www/obs/api/config/environments
%dir /srv/www/obs/api/files
%dir /srv/www/obs/api/db
/srv/www/obs/api/db/checker.rb
/srv/www/obs/api/Gemfile
%verify(not mtime) /srv/www/obs/api/last_deploy
/srv/www/obs/api/Gemfile.lock
/srv/www/obs/api/config.ru
/srv/www/obs/api/config/application.rb
/srv/www/obs/api/config/clock.rb
%config(noreplace) /etc/logrotate.d/obs-api
%{_unitdir}/obsapisetup.service
%{_unitdir}/obs-api-support.target
%{_unitdir}/obs-clockwork.service
%{_unitdir}/obs-delayedjob-queue-consistency_check.service
%{_unitdir}/obs-delayedjob-queue-default.service
%{_unitdir}/obs-delayedjob-queue-issuetracking.service
%{_unitdir}/obs-delayedjob-queue-mailers.service
%{_unitdir}/obs-delayedjob-queue-project_log_rotate.service
%{_unitdir}/obs-delayedjob-queue-quick@.service
%{_unitdir}/obs-delayedjob-queue-releasetracking.service
%{_unitdir}/obs-delayedjob-queue-staging.service
%{_unitdir}/obs-sphinx.service
%{_sbindir}/rcobs-api-support
%{_sbindir}/rcobs-clockwork
%{_sbindir}/rcobs-delayedjob-queue-consistency_check
%{_sbindir}/rcobs-delayedjob-queue-default
%{_sbindir}/rcobs-delayedjob-queue-issuetracking
%{_sbindir}/rcobs-delayedjob-queue-mailers
%{_sbindir}/rcobs-delayedjob-queue-project_log_rotate
%{_sbindir}/rcobs-delayedjob-queue-releasetracking
%{_sbindir}/rcobs-delayedjob-queue-staging
%{_sbindir}/rcobs-sphinx
%{_sbindir}/rcobsapisetup
/srv/www/obs/api/app
%attr(-,apache,apache)  /srv/www/obs/api/db/structure.sql
%attr(-,apache,apache)  /srv/www/obs/api/db/data_schema.rb
/srv/www/obs/api/db/attribute_descriptions.rb
/srv/www/obs/api/db/data
/srv/www/obs/api/db/migrate
/srv/www/obs/api/db/seeds.rb
/srv/www/obs/api/files/wizardtemplate.spec
/srv/www/obs/api/lib
/srv/www/obs/api/public
/srv/www/obs/api/Rakefile
/srv/www/obs/api/script
/srv/www/obs/api/bin
/srv/www/obs/api/test
/srv/www/obs/api/vendor/assets
/srv/www/obs/docs
/srv/www/obs/api/config/locales
%dir /srv/www/obs/api/vendor
/srv/www/obs/api/vendor/diststats
/srv/www/obs/api/config/boot.rb
/srv/www/obs/api/config/routes.rb
/srv/www/obs/api/config/environments/development.rb
%attr(0640,root,%apache_group) %config(noreplace) %verify(md5) /srv/www/obs/api/config/database.yml
%attr(0640,root,%apache_group) /srv/www/obs/api/config/database.yml.example
%attr(0644,root,root) %config(noreplace) %verify(md5) /srv/www/obs/api/config/options.yml
%attr(0644,root,root) /srv/www/obs/api/config/options.yml.example
%dir %attr(0755,%apache_user,%apache_group) /srv/www/obs/api/db/sphinx
%dir %attr(0755,%apache_user,%apache_group) /srv/www/obs/api/db/sphinx/production
/srv/www/obs/api/.bundle
%config /srv/www/obs/api/config/environment.rb
%config /srv/www/obs/api/config/environments/production.rb
%config /srv/www/obs/api/config/environments/test.rb
%config /srv/www/obs/api/config/environments/stage.rb
%dir %attr(-,apache,apache) /srv/www/obs/api/log
%attr(-,apache,apache) /srv/www/obs/api/tmp
%dir /etc/apache2
%dir /etc/apache2/vhosts.d
%config(noreplace) /etc/apache2/vhosts.d/obs.conf
%defattr(0644,wwwrun,www)
%ghost /srv/www/obs/api/log/access.log
%ghost /srv/www/obs/api/log/backend_access.log
%ghost /srv/www/obs/api/log/delayed_job.log
%ghost /srv/www/obs/api/log/error.log
%ghost /srv/www/obs/api/log/lastevents.access.log
%ghost /srv/www/obs/api/log/production.log
%ghost %attr(0640,root,www) /srv/www/obs/api/config/secret.key

%files -n obs-common
%defattr(-,root,root)
/var/adm/fillup-templates/sysconfig.obs-server
/usr/lib/obs/server/setup-appliance.sh
%{_unitdir}/obsstoragesetup.service
%{_sbindir}/obsstoragesetup
%{_sbindir}/rcobsstoragesetup

%changelog
* Mon Feb 13 2023 wangkai <wangkai385@h-partners.com> 2.10.15-5
- Modify the rdoc version to 6.3.3

* Wed Feb 8 2023 liyanan <liyanan32@h-partners.com> - 2.10.15-4
- Fix build and install failed

* Fri Jan 13 2023 Ge Wang <wangge20@h-partners.com> - 2.10.15-3
- Fix build failure due to version of pry and factory_bot mismatch

* Wed Jun 15 2022 wangkai <wangkai385@h-partners.com> - 2.10.15-2
- Fix obs-api install warning

* Wed May 25 2022 wangkai <wangkai385@h-partners.com> - 2.10.15-1
- Upgrade to version 2.10.15

* Mon May 16 2022 wangkai <wangkai385@h-partners.com> - 2.10.11-1
- Upgrade to version 2.10.11 and update xmlhash to 1.3.8 for fix CVE-2022-21949

* Thu Sep 23 2021 lingsheng <lingsheng@huawei.com> - 2.10.7-8
- fix obsapisetup script problems, modify production.sphinx.conf owner

* Sat Aug 21 2021 caodongxia <caodongxia@huawei.com> - 2.10.7-7
- fix obsscheduler and obsworker

* Wed Jun 9 2021 zhaomengchao <zhaomengchao3@huawei.com> - 2.10.7-6
- fix obs-server Compilation Failed

* Mon Dec 28 2020 xinghe <xinghe1@huawei.com> - 2.10.7-5
- fix obs_admin can't locate BSConfig.pm

* Tue Dec 8 2020 yanglongkang <yanglongkang@huawei.com> - 2.10.7-4
- Delete dependent software package version

* Fri Oct 30 2020 yanglongkang <yanglongkang@huawei.com> - 2.10.7-3
- remove python2 dependency

* Tue Oct 20 2020 wutao <wutao61@huawei.com> - 2.10.7-2
- delete extra tar and change yaml

* Mon Oct 19 2020 wutao <wutao61@huawei.com> - 2.10.7-1
- upgrade to 2.10.7

* Sun Sep 20 2020 yanan li <liyanan032@huawei.com> - 2.10.1-lp151.23.6
- delete %insserv_cleanup

* Fri Sep 18 2020 Zhiqiang Liu <liuzhiqiang26@huawei.com> - 2.10.1-lp151.23.5
- correct usage info when exec 'obs_serverstatus' without other pargs

* Fri Sep 18 2020 lingsheng <lingsheng@huawei.com> - 2.10.1-lp151.23.4
- Add missing macros

* Sun Mar 15 2020 hy-euler <eulerstoragemt@huawei.com> - 2.10.1-lp151.23.3
- delete the Provides of obs-cloud-uploader which provides the tools for upload images to the cloud

* Sat Mar 14 2020 hy-euler <eulerstoragemt@huawei.com> - 2.10.1-lp151.23.2
- delete the use of %{fillup_and_insserv} and %check, it is not necessary

* Sat Mar 7 2020 Ruijun Ge <geruijun@huawei.com> - 2.10.1-lp151.23.1
- Package init
